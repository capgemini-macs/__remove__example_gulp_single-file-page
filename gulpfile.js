var gulp = require('gulp');
var cssBase64 = require('gulp-css-base64');
var inlinesource = require('gulp-inline-source');

gulp.task('base64', function () {
    return gulp.src('css/style.css')
        .pipe(cssBase64({
            maxWeightResource: 10000000
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('inlinesource', ['base64'], function () {
    return gulp.src('index.html')
        .pipe(inlinesource())
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['base64', 'inlinesource']);