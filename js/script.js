$(function() {
    var max = 42;
    var current = 1;
    $('.image').click(function(){
        var next = current + 1 <= max ? current + 1 : 1;
        $('.image').removeClass('image'+current);
        $('.image').addClass('image'+next);
        current = next;
    });
});