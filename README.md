Single File Page
================

Quick example how to use Gulp to embed all JS, CSS and images into single HTML file.

Setup
-----

node.js and gulp are required.

To install gulp and required plugins locally in your command-line application, type:

    npm install

Build
-----

To build single file type:

    gulp

dist/index.html is your build file.